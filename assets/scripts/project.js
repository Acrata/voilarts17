'use strict';

/**
 * File js-enabled.js
 *
 * If Javascript is enabled, replace the <body> class "no-js".
 */
document.body.className = document.body.className.replace('no-js', 'js');
'use strict';

/**
 * File js-te
 *
 * TTE
 */
window.teMenu = {};

(function (window, $, app) {
	// Constructor.
	app.init = function () {
		app.cache();
		app.te();
		//if ( app.meetsRequirements() ) {
		//app.bindEvents();
		//}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			'body': $('body')
		};
	};

	app.te = function () {
		console.log("offman");
		$("#trigger p").click(function () {
			$(".wrapper-vv").toggleClass("is-open");
		});
	};

	// Do we meet the requirements?
	//app.meetsRequirements = function () {
	//return $( '.search-field' ).length;
	//};

	// Combine all events.
	//app.bindEvents = function () {
	// Remove placeholder text from search field on focus.
	//app.$c.body.on( 'focus', '.search-field', app.removePlaceholderText );

	// Add placeholder text back to search field on blur.
	//app.$c.body.on( 'blur', '.search-field', app.addPlaceholderText );
	//};

	// Remove placeholder text from search field.
	//app.removePlaceholderText = function () {
	//var $search_field = $( this );

	//$search_field.data( 'placeholder', $search_field.attr( 'placeholder' ) ).attr( 'placeholder', '' );
	//};

	// Replace placeholder text from search field.
	//app.addPlaceholderText = function () {
	//var $search_field = $( this );

	//$search_field.attr( 'placeholder', $search_field.data( 'placeholder' ) ).data( 'placeholder', '' );
	//};

	// Engage!
	$(app.init);
})(window, jQuery, window.teMenu);
'use strict';

/**
 * File modal.js
 *
 * Deal with multiple modals and their media.
 */
window.wdsModal = {};

(function (window, $, app) {
	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			'body': $('body')
		};
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return $('.modal-trigger').length;
	};

	// Combine all events.
	app.bindEvents = function () {
		// Trigger a modal to open.
		app.$c.body.on('click touchstart', '.modal-trigger', app.openModal);

		// Trigger the close button to close the modal.
		app.$c.body.on('click touchstart', '.close', app.closeModal);

		// Allow the user to close the modal by hitting the esc key.
		app.$c.body.on('keydown', app.escKeyClose);

		// Allow the user to close the modal by clicking outside of the modal.
		app.$c.body.on('click touchstart', 'div.modal-open', app.closeModalByClick);
	};

	// Open the modal.
	app.openModal = function () {
		// Figure out which modal we're opening and store the object.
		var $modal = $($(this).data('target'));

		// Display the modal.
		$modal.addClass('modal-open');

		// Add body class.
		app.$c.body.addClass('modal-open');
	};

	// Close the modal.
	app.closeModal = function () {
		// Figure the opened modal we're closing and store the object.
		var $modal = $($('div.modal-open .close').data('target'));

		// Find the iframe in the $modal object.
		var $iframe = $modal.find('iframe');

		// Get the iframe src URL.
		var url = $iframe.attr('src');

		// Remove the source URL, then add it back, so the video can be played again later.
		$iframe.attr('src', '').attr('src', url);

		// Finally, hide the modal.
		$modal.removeClass('modal-open');

		// Remove the body class.
		app.$c.body.removeClass('modal-open');
	};

	// Close if "esc" key is pressed.
	app.escKeyClose = function (event) {
		if (27 === event.keyCode) {
			app.closeModal();
		}
	};

	// Close if the user clicks outside of the modal
	app.closeModalByClick = function (event) {
		// If the parent container is NOT the modal dialog container, close the modal
		if (!$(event.target).parents('div').hasClass('modal-dialog')) {
			app.closeModal();
		}
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsModal);
'use strict';

/**
 * File search.js
 *
 * Deal with the search form.
 */
window.responsivevv = {};

(function (window, $, app) {
	// Constructor.
	app.init = function () {
		app.cache();
		app.menuParentTarget();
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			'body': $('body'),
			'menuParent': $('.menu-item-has-children')
		};
	};
	app.menuParentTarget = function () {
		// 		console.log(window.Hammer);
		// 		 app.$c.menuParent.on('touchstart', function(){event.preventDefault(); $(this).find('.sub-menu li').addClass('menu-hover-behavior'); console.log(this)} );
		// 			console.log("s");
		// 		var hammertime = new window.Hammer(app.$c.menuParent);
		// hammertime.on('tap', function(ev) {
		// 	console.log(ev);
		// });
		$('.artist-sub-item .sub-menu').addClass('dropdown-hide');
		$('.artist-sub-item a').mouseenter(function () {
			event.preventDefault();
			//$('.artist-sub-item .sub-menu').css('display','');
			// $(this).css('background','yellow');
			$(this).closest('.artist-sub-item').children(".sub-menu").addClass("dropdown-open").removeClass("dropdown-hide");
			console.log($(this));
		});
		$('.artist-sub-item .sub-menu').mouseleave(function () {
			event.preventDefault();
			$(this).css('background', 'yellow');
			//	$(this).children('.sub-menu').removeClass('dropdown-open').addClass('dropdown-hide');
			$(this).removeClass('dropdown-open').addClass('dropdown-hide');
			console.log($(this));
		});

		// window width is at least 500px
		// window width is less than 500px
	};
	// Do we meet the requirements?
	// // // app.meetsRequirements = function () {
	// // // 	return $( '.search-field' ).length;
	// // };
	// //
	// // // Combine all events.
	// // app.bindEvents = function () {
	// // 	// Remove placeholder text from search field on focus.
	// // 	app.$c.body.on( 'focus', '.search-field', app.removePlaceholderText );
	// //
	// 	// Add placeholder text back to search field on blur.
	// 	app.$c.body.on( 'blur', '.search-field', app.addPlaceholderText );
	// };
	//
	// // Remove placeholder text from search field.
	// app.removePlaceholderText = function () {
	// 	var $search_field = $( this );
	//
	// 	$search_field.data( 'placeholder', $search_field.attr( 'placeholder' ) ).attr( 'placeholder', '' );
	// };
	//
	// // Replace placeholder text from search field.
	// app.addPlaceholderText = function () {
	// 	var $search_field = $( this );
	//
	// 	$search_field.attr( 'placeholder', $search_field.data( 'placeholder' ) ).data( 'placeholder', '' );
	// };
	//
	// Engage!
	$(app.init);
})(window, jQuery, window.responsivevv);
'use strict';

/**
 * File search.js
 *
 * Deal with the search form.
 */
window.wdsSearch = {};

(function (window, $, app) {
	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			'body': $('body')
		};
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return $('.search-field').length;
	};

	// Combine all events.
	app.bindEvents = function () {
		// Remove placeholder text from search field on focus.
		app.$c.body.on('focus', '.search-field', app.removePlaceholderText);

		// Add placeholder text back to search field on blur.
		app.$c.body.on('blur', '.search-field', app.addPlaceholderText);
	};

	// Remove placeholder text from search field.
	app.removePlaceholderText = function () {
		var $search_field = $(this);

		$search_field.data('placeholder', $search_field.attr('placeholder')).attr('placeholder', '');
	};

	// Replace placeholder text from search field.
	app.addPlaceholderText = function () {
		var $search_field = $(this);

		$search_field.attr('placeholder', $search_field.data('placeholder')).data('placeholder', '');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsSearch);
'use strict';

/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
(function () {
	var isWebkit = navigator.userAgent.toLowerCase().indexOf('webkit') > -1,
	    isOpera = navigator.userAgent.toLowerCase().indexOf('opera') > -1,
	    isIe = navigator.userAgent.toLowerCase().indexOf('msie') > -1;

	if ((isWebkit || isOpera || isIe) && document.getElementById && window.addEventListener) {
		window.addEventListener('hashchange', function () {
			var id = location.hash.substring(1),
			    element;

			if (!/^[A-z0-9_-]+$/.test(id)) {
				return;
			}

			element = document.getElementById(id);

			if (element) {
				if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName)) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false);
	}
})();
'use strict';

/**
 * File window-ready.js
 *
 * Add a "ready" class to <body> when window is ready.
 */
window.wdsWindowReady = {};
(function (window, $, app) {
	// Constructor.
	app.init = function () {
		app.cache();
		app.bindEvents();
	};

	// Cache document elements.
	app.cache = function () {
		app.$c = {
			'window': $(window),
			'body': $(document.body)
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.load(app.addBodyClass);
	};

	// Add a class to <body>.
	app.addBodyClass = function () {
		app.$c.body.addClass('ready');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsWindowReady);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpzLWVuYWJsZWQuanMiLCJtZW51LXZvaWwuanMiLCJtb2RhbC5qcyIsInJlc3BvbnNpdmUuanMiLCJzZWFyY2guanMiLCJza2lwLWxpbmstZm9jdXMtZml4LmpzIiwid2luZG93LXJlYWR5LmpzIl0sIm5hbWVzIjpbImRvY3VtZW50IiwiYm9keSIsImNsYXNzTmFtZSIsInJlcGxhY2UiLCJ3aW5kb3ciLCJ0ZU1lbnUiLCIkIiwiYXBwIiwiaW5pdCIsImNhY2hlIiwidGUiLCIkYyIsImNvbnNvbGUiLCJsb2ciLCJjbGljayIsInRvZ2dsZUNsYXNzIiwialF1ZXJ5Iiwid2RzTW9kYWwiLCJtZWV0c1JlcXVpcmVtZW50cyIsImJpbmRFdmVudHMiLCJsZW5ndGgiLCJvbiIsIm9wZW5Nb2RhbCIsImNsb3NlTW9kYWwiLCJlc2NLZXlDbG9zZSIsImNsb3NlTW9kYWxCeUNsaWNrIiwiJG1vZGFsIiwiZGF0YSIsImFkZENsYXNzIiwiJGlmcmFtZSIsImZpbmQiLCJ1cmwiLCJhdHRyIiwicmVtb3ZlQ2xhc3MiLCJldmVudCIsImtleUNvZGUiLCJ0YXJnZXQiLCJwYXJlbnRzIiwiaGFzQ2xhc3MiLCJyZXNwb25zaXZldnYiLCJtZW51UGFyZW50VGFyZ2V0IiwibW91c2VlbnRlciIsInByZXZlbnREZWZhdWx0IiwiY2xvc2VzdCIsImNoaWxkcmVuIiwibW91c2VsZWF2ZSIsImNzcyIsIndkc1NlYXJjaCIsInJlbW92ZVBsYWNlaG9sZGVyVGV4dCIsImFkZFBsYWNlaG9sZGVyVGV4dCIsIiRzZWFyY2hfZmllbGQiLCJpc1dlYmtpdCIsIm5hdmlnYXRvciIsInVzZXJBZ2VudCIsInRvTG93ZXJDYXNlIiwiaW5kZXhPZiIsImlzT3BlcmEiLCJpc0llIiwiZ2V0RWxlbWVudEJ5SWQiLCJhZGRFdmVudExpc3RlbmVyIiwiaWQiLCJsb2NhdGlvbiIsImhhc2giLCJzdWJzdHJpbmciLCJlbGVtZW50IiwidGVzdCIsInRhZ05hbWUiLCJ0YWJJbmRleCIsImZvY3VzIiwid2RzV2luZG93UmVhZHkiLCJsb2FkIiwiYWRkQm9keUNsYXNzIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7OztBQUtDQSxTQUFTQyxJQUFULENBQWNDLFNBQWQsR0FBMEJGLFNBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkMsT0FBeEIsQ0FBaUMsT0FBakMsRUFBMEMsSUFBMUMsQ0FBMUI7OztBQ0xEOzs7OztBQUtBQyxPQUFPQyxNQUFQLEdBQWdCLEVBQWhCOztBQUVBLENBQUUsVUFBV0QsTUFBWCxFQUFtQkUsQ0FBbkIsRUFBc0JDLEdBQXRCLEVBQTRCO0FBQzdCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFZO0FBQ3RCRCxNQUFJRSxLQUFKO0FBQ01GLE1BQUlHLEVBQUo7QUFDTjtBQUNDO0FBQ0Q7QUFDQSxFQU5EOztBQVFBO0FBQ0FILEtBQUlFLEtBQUosR0FBWSxZQUFZO0FBQ3ZCRixNQUFJSSxFQUFKLEdBQVM7QUFDUixXQUFRTCxFQUFHLE1BQUg7QUFEQSxHQUFUO0FBR0EsRUFKRDs7QUFNR0MsS0FBSUcsRUFBSixHQUFTLFlBQVk7QUFDakJFLFVBQVFDLEdBQVIsQ0FBWSxRQUFaO0FBQ1JQLElBQUcsWUFBSCxFQUFrQlEsS0FBbEIsQ0FBd0IsWUFBVztBQUNqQ1IsS0FBRSxhQUFGLEVBQWlCUyxXQUFqQixDQUE2QixTQUE3QjtBQUNELEdBRkQ7QUFHSyxFQUxEOztBQU9IO0FBQ0E7QUFDQztBQUNEOztBQUVBO0FBQ0E7QUFDQztBQUNBOztBQUVBO0FBQ0E7QUFDRDs7QUFFQTtBQUNBO0FBQ0M7O0FBRUE7QUFDRDs7QUFFQTtBQUNBO0FBQ0M7O0FBRUE7QUFDRDs7QUFFQTtBQUNBVCxHQUFHQyxJQUFJQyxJQUFQO0FBQ0EsQ0F0REQsRUFzREtKLE1BdERMLEVBc0RhWSxNQXREYixFQXNEcUJaLE9BQU9DLE1BdEQ1Qjs7O0FDUEE7Ozs7O0FBS0FELE9BQU9hLFFBQVAsR0FBa0IsRUFBbEI7O0FBRUEsQ0FBRSxVQUFXYixNQUFYLEVBQW1CRSxDQUFuQixFQUFzQkMsR0FBdEIsRUFBNEI7QUFDN0I7QUFDQUEsS0FBSUMsSUFBSixHQUFXLFlBQVk7QUFDdEJELE1BQUlFLEtBQUo7O0FBRUEsTUFBS0YsSUFBSVcsaUJBQUosRUFBTCxFQUErQjtBQUM5QlgsT0FBSVksVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBWixLQUFJRSxLQUFKLEdBQVksWUFBWTtBQUN2QkYsTUFBSUksRUFBSixHQUFTO0FBQ1IsV0FBUUwsRUFBRyxNQUFIO0FBREEsR0FBVDtBQUdBLEVBSkQ7O0FBTUE7QUFDQUMsS0FBSVcsaUJBQUosR0FBd0IsWUFBWTtBQUNuQyxTQUFPWixFQUFHLGdCQUFILEVBQXNCYyxNQUE3QjtBQUNBLEVBRkQ7O0FBSUE7QUFDQWIsS0FBSVksVUFBSixHQUFpQixZQUFZO0FBQzVCO0FBQ0FaLE1BQUlJLEVBQUosQ0FBT1YsSUFBUCxDQUFZb0IsRUFBWixDQUFnQixrQkFBaEIsRUFBb0MsZ0JBQXBDLEVBQXNEZCxJQUFJZSxTQUExRDs7QUFFQTtBQUNBZixNQUFJSSxFQUFKLENBQU9WLElBQVAsQ0FBWW9CLEVBQVosQ0FBZ0Isa0JBQWhCLEVBQW9DLFFBQXBDLEVBQThDZCxJQUFJZ0IsVUFBbEQ7O0FBRUE7QUFDQWhCLE1BQUlJLEVBQUosQ0FBT1YsSUFBUCxDQUFZb0IsRUFBWixDQUFnQixTQUFoQixFQUEyQmQsSUFBSWlCLFdBQS9COztBQUVBO0FBQ0FqQixNQUFJSSxFQUFKLENBQU9WLElBQVAsQ0FBWW9CLEVBQVosQ0FBZ0Isa0JBQWhCLEVBQW9DLGdCQUFwQyxFQUFzRGQsSUFBSWtCLGlCQUExRDtBQUNBLEVBWkQ7O0FBY0E7QUFDQWxCLEtBQUllLFNBQUosR0FBZ0IsWUFBWTtBQUMzQjtBQUNBLE1BQUlJLFNBQVNwQixFQUFHQSxFQUFHLElBQUgsRUFBVXFCLElBQVYsQ0FBZ0IsUUFBaEIsQ0FBSCxDQUFiOztBQUVBO0FBQ0FELFNBQU9FLFFBQVAsQ0FBaUIsWUFBakI7O0FBRUE7QUFDQXJCLE1BQUlJLEVBQUosQ0FBT1YsSUFBUCxDQUFZMkIsUUFBWixDQUFzQixZQUF0QjtBQUNBLEVBVEQ7O0FBV0E7QUFDQXJCLEtBQUlnQixVQUFKLEdBQWlCLFlBQVk7QUFDNUI7QUFDQSxNQUFJRyxTQUFTcEIsRUFBR0EsRUFBRyx1QkFBSCxFQUE2QnFCLElBQTdCLENBQW1DLFFBQW5DLENBQUgsQ0FBYjs7QUFFQTtBQUNBLE1BQUlFLFVBQVVILE9BQU9JLElBQVAsQ0FBYSxRQUFiLENBQWQ7O0FBRUE7QUFDQSxNQUFJQyxNQUFNRixRQUFRRyxJQUFSLENBQWMsS0FBZCxDQUFWOztBQUVBO0FBQ0FILFVBQVFHLElBQVIsQ0FBYyxLQUFkLEVBQXFCLEVBQXJCLEVBQTBCQSxJQUExQixDQUFnQyxLQUFoQyxFQUF1Q0QsR0FBdkM7O0FBRUE7QUFDQUwsU0FBT08sV0FBUCxDQUFvQixZQUFwQjs7QUFFQTtBQUNBMUIsTUFBSUksRUFBSixDQUFPVixJQUFQLENBQVlnQyxXQUFaLENBQXlCLFlBQXpCO0FBQ0EsRUFsQkQ7O0FBb0JBO0FBQ0ExQixLQUFJaUIsV0FBSixHQUFrQixVQUFXVSxLQUFYLEVBQW1CO0FBQ3BDLE1BQUssT0FBT0EsTUFBTUMsT0FBbEIsRUFBNEI7QUFDM0I1QixPQUFJZ0IsVUFBSjtBQUNBO0FBQ0QsRUFKRDs7QUFNQTtBQUNBaEIsS0FBSWtCLGlCQUFKLEdBQXdCLFVBQVdTLEtBQVgsRUFBbUI7QUFDMUM7QUFDQSxNQUFLLENBQUM1QixFQUFHNEIsTUFBTUUsTUFBVCxFQUFrQkMsT0FBbEIsQ0FBMkIsS0FBM0IsRUFBbUNDLFFBQW5DLENBQTZDLGNBQTdDLENBQU4sRUFBc0U7QUFDckUvQixPQUFJZ0IsVUFBSjtBQUNBO0FBQ0QsRUFMRDs7QUFPQTtBQUNBakIsR0FBR0MsSUFBSUMsSUFBUDtBQUNBLENBdkZELEVBdUZLSixNQXZGTCxFQXVGYVksTUF2RmIsRUF1RnFCWixPQUFPYSxRQXZGNUI7OztBQ1BBOzs7OztBQUtBYixPQUFPbUMsWUFBUCxHQUFzQixFQUF0Qjs7QUFFQSxDQUFFLFVBQVduQyxNQUFYLEVBQW1CRSxDQUFuQixFQUFzQkMsR0FBdEIsRUFBMkI7QUFDNUI7QUFDQUEsS0FBSUMsSUFBSixHQUFXLFlBQVk7QUFDdEJELE1BQUlFLEtBQUo7QUFDQUYsTUFBSWlDLGdCQUFKO0FBQ0EsRUFIRDs7QUFLQTtBQUNBakMsS0FBSUUsS0FBSixHQUFZLFlBQVk7QUFDdkJGLE1BQUlJLEVBQUosR0FBUztBQUNSLFdBQVFMLEVBQUcsTUFBSCxDQURBO0FBRVIsaUJBQWNBLEVBQUUseUJBQUY7QUFGTixHQUFUO0FBSUEsRUFMRDtBQU1BQyxLQUFJaUMsZ0JBQUosR0FBdUIsWUFBWTtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNDbEMsSUFBRSw0QkFBRixFQUFnQ3NCLFFBQWhDLENBQXlDLGVBQXpDO0FBQ0R0QixJQUFFLG9CQUFGLEVBQXdCbUMsVUFBeEIsQ0FBbUMsWUFBWTtBQUM5Q1AsU0FBTVEsY0FBTjtBQUNDO0FBQ0U7QUFDRnBDLEtBQUUsSUFBRixFQUFRcUMsT0FBUixDQUFnQixrQkFBaEIsRUFBb0NDLFFBQXBDLENBQTZDLFdBQTdDLEVBQTBEaEIsUUFBMUQsQ0FBbUUsZUFBbkUsRUFBb0ZLLFdBQXBGLENBQWdHLGVBQWhHO0FBQ0RyQixXQUFRQyxHQUFSLENBQVlQLEVBQUUsSUFBRixDQUFaO0FBRUEsR0FQRDtBQVFBQSxJQUFFLDRCQUFGLEVBQWdDdUMsVUFBaEMsQ0FBMkMsWUFBWTtBQUN0RFgsU0FBTVEsY0FBTjtBQUNJcEMsS0FBRSxJQUFGLEVBQVF3QyxHQUFSLENBQVksWUFBWixFQUF5QixRQUF6QjtBQUNKO0FBQ0N4QyxLQUFFLElBQUYsRUFBUTJCLFdBQVIsQ0FBb0IsZUFBcEIsRUFBcUNMLFFBQXJDLENBQThDLGVBQTlDO0FBQ0RoQixXQUFRQyxHQUFSLENBQVlQLEVBQUUsSUFBRixDQUFaO0FBRUEsR0FQRDs7QUFTRTtBQUNBO0FBQ0QsRUE1QkE7QUE2QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBQSxHQUFHQyxJQUFJQyxJQUFQO0FBQ0EsQ0F6RUQsRUF5RUtKLE1BekVMLEVBeUVhWSxNQXpFYixFQXlFcUJaLE9BQU9tQyxZQXpFNUI7OztBQ1BBOzs7OztBQUtBbkMsT0FBTzJDLFNBQVAsR0FBbUIsRUFBbkI7O0FBRUEsQ0FBRSxVQUFXM0MsTUFBWCxFQUFtQkUsQ0FBbkIsRUFBc0JDLEdBQXRCLEVBQTRCO0FBQzdCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFZO0FBQ3RCRCxNQUFJRSxLQUFKOztBQUVBLE1BQUtGLElBQUlXLGlCQUFKLEVBQUwsRUFBK0I7QUFDOUJYLE9BQUlZLFVBQUo7QUFDQTtBQUNELEVBTkQ7O0FBUUE7QUFDQVosS0FBSUUsS0FBSixHQUFZLFlBQVk7QUFDdkJGLE1BQUlJLEVBQUosR0FBUztBQUNSLFdBQVFMLEVBQUcsTUFBSDtBQURBLEdBQVQ7QUFHQSxFQUpEOztBQU1BO0FBQ0FDLEtBQUlXLGlCQUFKLEdBQXdCLFlBQVk7QUFDbkMsU0FBT1osRUFBRyxlQUFILEVBQXFCYyxNQUE1QjtBQUNBLEVBRkQ7O0FBSUE7QUFDQWIsS0FBSVksVUFBSixHQUFpQixZQUFZO0FBQzVCO0FBQ0FaLE1BQUlJLEVBQUosQ0FBT1YsSUFBUCxDQUFZb0IsRUFBWixDQUFnQixPQUFoQixFQUF5QixlQUF6QixFQUEwQ2QsSUFBSXlDLHFCQUE5Qzs7QUFFQTtBQUNBekMsTUFBSUksRUFBSixDQUFPVixJQUFQLENBQVlvQixFQUFaLENBQWdCLE1BQWhCLEVBQXdCLGVBQXhCLEVBQXlDZCxJQUFJMEMsa0JBQTdDO0FBQ0EsRUFORDs7QUFRQTtBQUNBMUMsS0FBSXlDLHFCQUFKLEdBQTRCLFlBQVk7QUFDdkMsTUFBSUUsZ0JBQWdCNUMsRUFBRyxJQUFILENBQXBCOztBQUVBNEMsZ0JBQWN2QixJQUFkLENBQW9CLGFBQXBCLEVBQW1DdUIsY0FBY2xCLElBQWQsQ0FBb0IsYUFBcEIsQ0FBbkMsRUFBeUVBLElBQXpFLENBQStFLGFBQS9FLEVBQThGLEVBQTlGO0FBQ0EsRUFKRDs7QUFNQTtBQUNBekIsS0FBSTBDLGtCQUFKLEdBQXlCLFlBQVk7QUFDcEMsTUFBSUMsZ0JBQWdCNUMsRUFBRyxJQUFILENBQXBCOztBQUVBNEMsZ0JBQWNsQixJQUFkLENBQW9CLGFBQXBCLEVBQW1Da0IsY0FBY3ZCLElBQWQsQ0FBb0IsYUFBcEIsQ0FBbkMsRUFBeUVBLElBQXpFLENBQStFLGFBQS9FLEVBQThGLEVBQTlGO0FBQ0EsRUFKRDs7QUFNQTtBQUNBckIsR0FBR0MsSUFBSUMsSUFBUDtBQUNBLENBL0NELEVBK0NLSixNQS9DTCxFQStDYVksTUEvQ2IsRUErQ3FCWixPQUFPMkMsU0EvQzVCOzs7QUNQQTs7Ozs7OztBQU9BLENBQUUsWUFBWTtBQUNiLEtBQUlJLFdBQVdDLFVBQVVDLFNBQVYsQ0FBb0JDLFdBQXBCLEdBQWtDQyxPQUFsQyxDQUEyQyxRQUEzQyxJQUF3RCxDQUFDLENBQXhFO0FBQUEsS0FDQ0MsVUFBVUosVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTJDLE9BQTNDLElBQXVELENBQUMsQ0FEbkU7QUFBQSxLQUVDRSxPQUFPTCxVQUFVQyxTQUFWLENBQW9CQyxXQUFwQixHQUFrQ0MsT0FBbEMsQ0FBMkMsTUFBM0MsSUFBc0QsQ0FBQyxDQUYvRDs7QUFJQSxLQUFLLENBQUVKLFlBQVlLLE9BQVosSUFBdUJDLElBQXpCLEtBQW1DekQsU0FBUzBELGNBQTVDLElBQThEdEQsT0FBT3VELGdCQUExRSxFQUE2RjtBQUM1RnZELFNBQU91RCxnQkFBUCxDQUF5QixZQUF6QixFQUF1QyxZQUFZO0FBQ2xELE9BQUlDLEtBQUtDLFNBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF5QixDQUF6QixDQUFUO0FBQUEsT0FDQ0MsT0FERDs7QUFHQSxPQUFLLENBQUcsZUFBRixDQUFvQkMsSUFBcEIsQ0FBMEJMLEVBQTFCLENBQU4sRUFBdUM7QUFDdEM7QUFDQTs7QUFFREksYUFBVWhFLFNBQVMwRCxjQUFULENBQXlCRSxFQUF6QixDQUFWOztBQUVBLE9BQUtJLE9BQUwsRUFBZTtBQUNkLFFBQUssQ0FBRyx1Q0FBRixDQUE0Q0MsSUFBNUMsQ0FBa0RELFFBQVFFLE9BQTFELENBQU4sRUFBNEU7QUFDM0VGLGFBQVFHLFFBQVIsR0FBbUIsQ0FBQyxDQUFwQjtBQUNBOztBQUVESCxZQUFRSSxLQUFSO0FBQ0E7QUFDRCxHQWpCRCxFQWlCRyxLQWpCSDtBQWtCQTtBQUNELENBekJEOzs7QUNQQTs7Ozs7QUFLQWhFLE9BQU9pRSxjQUFQLEdBQXdCLEVBQXhCO0FBQ0EsQ0FBRSxVQUFXakUsTUFBWCxFQUFtQkUsQ0FBbkIsRUFBc0JDLEdBQXRCLEVBQTRCO0FBQzdCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFZO0FBQ3RCRCxNQUFJRSxLQUFKO0FBQ0FGLE1BQUlZLFVBQUo7QUFDQSxFQUhEOztBQUtBO0FBQ0FaLEtBQUlFLEtBQUosR0FBWSxZQUFZO0FBQ3ZCRixNQUFJSSxFQUFKLEdBQVM7QUFDUixhQUFVTCxFQUFHRixNQUFILENBREY7QUFFUixXQUFRRSxFQUFHTixTQUFTQyxJQUFaO0FBRkEsR0FBVDtBQUlBLEVBTEQ7O0FBT0E7QUFDQU0sS0FBSVksVUFBSixHQUFpQixZQUFZO0FBQzVCWixNQUFJSSxFQUFKLENBQU9QLE1BQVAsQ0FBY2tFLElBQWQsQ0FBb0IvRCxJQUFJZ0UsWUFBeEI7QUFDQSxFQUZEOztBQUlBO0FBQ0FoRSxLQUFJZ0UsWUFBSixHQUFtQixZQUFZO0FBQzlCaEUsTUFBSUksRUFBSixDQUFPVixJQUFQLENBQVkyQixRQUFaLENBQXNCLE9BQXRCO0FBQ0EsRUFGRDs7QUFJQTtBQUNBdEIsR0FBR0MsSUFBSUMsSUFBUDtBQUNBLENBM0JELEVBMkJLSixNQTNCTCxFQTJCYVksTUEzQmIsRUEyQnFCWixPQUFPaUUsY0EzQjVCIiwiZmlsZSI6InByb2plY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEZpbGUganMtZW5hYmxlZC5qc1xuICpcbiAqIElmIEphdmFzY3JpcHQgaXMgZW5hYmxlZCwgcmVwbGFjZSB0aGUgPGJvZHk+IGNsYXNzIFwibm8tanNcIi5cbiAqL1xuIGRvY3VtZW50LmJvZHkuY2xhc3NOYW1lID0gZG9jdW1lbnQuYm9keS5jbGFzc05hbWUucmVwbGFjZSggJ25vLWpzJywgJ2pzJyApO1xuIiwiLyoqXG4gKiBGaWxlIGpzLXRlXG4gKlxuICogVFRFXG4gKi9cbndpbmRvdy50ZU1lbnUgPSB7fTtcblxuKCBmdW5jdGlvbiAoIHdpbmRvdywgJCwgYXBwICkge1xuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLmNhY2hlKCk7XG4gICAgICAgIGFwcC50ZSgpO1xuXHRcdC8vaWYgKCBhcHAubWVldHNSZXF1aXJlbWVudHMoKSApIHtcblx0XHRcdC8vYXBwLmJpbmRFdmVudHMoKTtcblx0XHQvL31cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC4kYyA9IHtcblx0XHRcdCdib2R5JzogJCggJ2JvZHknIClcblx0XHR9O1xuXHR9O1xuXG4gICAgYXBwLnRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBjb25zb2xlLmxvZyhcIm9mZm1hblwiKTtcbiQoIFwiI3RyaWdnZXIgcFwiICkuY2xpY2soZnVuY3Rpb24oKSB7XG4gICQoXCIud3JhcHBlci12dlwiKS50b2dnbGVDbGFzcyhcImlzLW9wZW5cIik7XG59KTtcbiAgICB9XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHQvL2FwcC5tZWV0c1JlcXVpcmVtZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHQvL3JldHVybiAkKCAnLnNlYXJjaC1maWVsZCcgKS5sZW5ndGg7XG5cdC8vfTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdC8vYXBwLmJpbmRFdmVudHMgPSBmdW5jdGlvbiAoKSB7XG5cdFx0Ly8gUmVtb3ZlIHBsYWNlaG9sZGVyIHRleHQgZnJvbSBzZWFyY2ggZmllbGQgb24gZm9jdXMuXG5cdFx0Ly9hcHAuJGMuYm9keS5vbiggJ2ZvY3VzJywgJy5zZWFyY2gtZmllbGQnLCBhcHAucmVtb3ZlUGxhY2Vob2xkZXJUZXh0ICk7XG5cblx0XHQvLyBBZGQgcGxhY2Vob2xkZXIgdGV4dCBiYWNrIHRvIHNlYXJjaCBmaWVsZCBvbiBibHVyLlxuXHRcdC8vYXBwLiRjLmJvZHkub24oICdibHVyJywgJy5zZWFyY2gtZmllbGQnLCBhcHAuYWRkUGxhY2Vob2xkZXJUZXh0ICk7XG5cdC8vfTtcblxuXHQvLyBSZW1vdmUgcGxhY2Vob2xkZXIgdGV4dCBmcm9tIHNlYXJjaCBmaWVsZC5cblx0Ly9hcHAucmVtb3ZlUGxhY2Vob2xkZXJUZXh0ID0gZnVuY3Rpb24gKCkge1xuXHRcdC8vdmFyICRzZWFyY2hfZmllbGQgPSAkKCB0aGlzICk7XG5cblx0XHQvLyRzZWFyY2hfZmllbGQuZGF0YSggJ3BsYWNlaG9sZGVyJywgJHNlYXJjaF9maWVsZC5hdHRyKCAncGxhY2Vob2xkZXInICkgKS5hdHRyKCAncGxhY2Vob2xkZXInLCAnJyApO1xuXHQvL307XG5cblx0Ly8gUmVwbGFjZSBwbGFjZWhvbGRlciB0ZXh0IGZyb20gc2VhcmNoIGZpZWxkLlxuXHQvL2FwcC5hZGRQbGFjZWhvbGRlclRleHQgPSBmdW5jdGlvbiAoKSB7XG5cdFx0Ly92YXIgJHNlYXJjaF9maWVsZCA9ICQoIHRoaXMgKTtcblxuXHRcdC8vJHNlYXJjaF9maWVsZC5hdHRyKCAncGxhY2Vob2xkZXInLCAkc2VhcmNoX2ZpZWxkLmRhdGEoICdwbGFjZWhvbGRlcicgKSApLmRhdGEoICdwbGFjZWhvbGRlcicsICcnICk7XG5cdC8vfTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG59ICkoIHdpbmRvdywgalF1ZXJ5LCB3aW5kb3cudGVNZW51ICk7XG4iLCIvKipcbiAqIEZpbGUgbW9kYWwuanNcbiAqXG4gKiBEZWFsIHdpdGggbXVsdGlwbGUgbW9kYWxzIGFuZCB0aGVpciBtZWRpYS5cbiAqL1xud2luZG93Lndkc01vZGFsID0ge307XG5cbiggZnVuY3Rpb24gKCB3aW5kb3csICQsIGFwcCApIHtcblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXG5cdFx0aWYgKCBhcHAubWVldHNSZXF1aXJlbWVudHMoKSApIHtcblx0XHRcdGFwcC5iaW5kRXZlbnRzKCk7XG5cdFx0fVxuXHR9O1xuXG5cdC8vIENhY2hlIGFsbCB0aGUgdGhpbmdzLlxuXHRhcHAuY2FjaGUgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0J2JvZHknOiAkKCAnYm9keScgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbiAoKSB7XG5cdFx0cmV0dXJuICQoICcubW9kYWwtdHJpZ2dlcicgKS5sZW5ndGg7XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHQvLyBUcmlnZ2VyIGEgbW9kYWwgdG8gb3Blbi5cblx0XHRhcHAuJGMuYm9keS5vbiggJ2NsaWNrIHRvdWNoc3RhcnQnLCAnLm1vZGFsLXRyaWdnZXInLCBhcHAub3Blbk1vZGFsICk7XG5cblx0XHQvLyBUcmlnZ2VyIHRoZSBjbG9zZSBidXR0b24gdG8gY2xvc2UgdGhlIG1vZGFsLlxuXHRcdGFwcC4kYy5ib2R5Lm9uKCAnY2xpY2sgdG91Y2hzdGFydCcsICcuY2xvc2UnLCBhcHAuY2xvc2VNb2RhbCApO1xuXG5cdFx0Ly8gQWxsb3cgdGhlIHVzZXIgdG8gY2xvc2UgdGhlIG1vZGFsIGJ5IGhpdHRpbmcgdGhlIGVzYyBrZXkuXG5cdFx0YXBwLiRjLmJvZHkub24oICdrZXlkb3duJywgYXBwLmVzY0tleUNsb3NlICk7XG5cblx0XHQvLyBBbGxvdyB0aGUgdXNlciB0byBjbG9zZSB0aGUgbW9kYWwgYnkgY2xpY2tpbmcgb3V0c2lkZSBvZiB0aGUgbW9kYWwuXG5cdFx0YXBwLiRjLmJvZHkub24oICdjbGljayB0b3VjaHN0YXJ0JywgJ2Rpdi5tb2RhbC1vcGVuJywgYXBwLmNsb3NlTW9kYWxCeUNsaWNrICk7XG5cdH07XG5cblx0Ly8gT3BlbiB0aGUgbW9kYWwuXG5cdGFwcC5vcGVuTW9kYWwgPSBmdW5jdGlvbiAoKSB7XG5cdFx0Ly8gRmlndXJlIG91dCB3aGljaCBtb2RhbCB3ZSdyZSBvcGVuaW5nIGFuZCBzdG9yZSB0aGUgb2JqZWN0LlxuXHRcdHZhciAkbW9kYWwgPSAkKCAkKCB0aGlzICkuZGF0YSggJ3RhcmdldCcgKSApO1xuXG5cdFx0Ly8gRGlzcGxheSB0aGUgbW9kYWwuXG5cdFx0JG1vZGFsLmFkZENsYXNzKCAnbW9kYWwtb3BlbicgKTtcblxuXHRcdC8vIEFkZCBib2R5IGNsYXNzLlxuXHRcdGFwcC4kYy5ib2R5LmFkZENsYXNzKCAnbW9kYWwtb3BlbicgKTtcblx0fTtcblxuXHQvLyBDbG9zZSB0aGUgbW9kYWwuXG5cdGFwcC5jbG9zZU1vZGFsID0gZnVuY3Rpb24gKCkge1xuXHRcdC8vIEZpZ3VyZSB0aGUgb3BlbmVkIG1vZGFsIHdlJ3JlIGNsb3NpbmcgYW5kIHN0b3JlIHRoZSBvYmplY3QuXG5cdFx0dmFyICRtb2RhbCA9ICQoICQoICdkaXYubW9kYWwtb3BlbiAuY2xvc2UnICkuZGF0YSggJ3RhcmdldCcgKSApO1xuXG5cdFx0Ly8gRmluZCB0aGUgaWZyYW1lIGluIHRoZSAkbW9kYWwgb2JqZWN0LlxuXHRcdHZhciAkaWZyYW1lID0gJG1vZGFsLmZpbmQoICdpZnJhbWUnICk7XG5cblx0XHQvLyBHZXQgdGhlIGlmcmFtZSBzcmMgVVJMLlxuXHRcdHZhciB1cmwgPSAkaWZyYW1lLmF0dHIoICdzcmMnICk7XG5cblx0XHQvLyBSZW1vdmUgdGhlIHNvdXJjZSBVUkwsIHRoZW4gYWRkIGl0IGJhY2ssIHNvIHRoZSB2aWRlbyBjYW4gYmUgcGxheWVkIGFnYWluIGxhdGVyLlxuXHRcdCRpZnJhbWUuYXR0ciggJ3NyYycsICcnICkuYXR0ciggJ3NyYycsIHVybCApO1xuXG5cdFx0Ly8gRmluYWxseSwgaGlkZSB0aGUgbW9kYWwuXG5cdFx0JG1vZGFsLnJlbW92ZUNsYXNzKCAnbW9kYWwtb3BlbicgKTtcblxuXHRcdC8vIFJlbW92ZSB0aGUgYm9keSBjbGFzcy5cblx0XHRhcHAuJGMuYm9keS5yZW1vdmVDbGFzcyggJ21vZGFsLW9wZW4nICk7XG5cdH07XG5cblx0Ly8gQ2xvc2UgaWYgXCJlc2NcIiBrZXkgaXMgcHJlc3NlZC5cblx0YXBwLmVzY0tleUNsb3NlID0gZnVuY3Rpb24gKCBldmVudCApIHtcblx0XHRpZiAoIDI3ID09PSBldmVudC5rZXlDb2RlICkge1xuXHRcdFx0YXBwLmNsb3NlTW9kYWwoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gQ2xvc2UgaWYgdGhlIHVzZXIgY2xpY2tzIG91dHNpZGUgb2YgdGhlIG1vZGFsXG5cdGFwcC5jbG9zZU1vZGFsQnlDbGljayA9IGZ1bmN0aW9uICggZXZlbnQgKSB7XG5cdFx0Ly8gSWYgdGhlIHBhcmVudCBjb250YWluZXIgaXMgTk9UIHRoZSBtb2RhbCBkaWFsb2cgY29udGFpbmVyLCBjbG9zZSB0aGUgbW9kYWxcblx0XHRpZiAoICEkKCBldmVudC50YXJnZXQgKS5wYXJlbnRzKCAnZGl2JyApLmhhc0NsYXNzKCAnbW9kYWwtZGlhbG9nJyApICkge1xuXHRcdFx0YXBwLmNsb3NlTW9kYWwoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xufSApKCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc01vZGFsICk7XG4iLCIvKipcbiAqIEZpbGUgc2VhcmNoLmpzXG4gKlxuICogRGVhbCB3aXRoIHRoZSBzZWFyY2ggZm9ybS5cbiAqL1xud2luZG93LnJlc3BvbnNpdmV2diA9IHt9O1xuXG4oIGZ1bmN0aW9uICggd2luZG93LCAkICxhcHApIHtcblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXHRcdGFwcC5tZW51UGFyZW50VGFyZ2V0KCk7XG5cdH07XG5cblx0Ly8gQ2FjaGUgYWxsIHRoZSB0aGluZ3MuXG5cdGFwcC5jYWNoZSA9IGZ1bmN0aW9uICgpIHtcblx0XHRhcHAuJGMgPSB7XG5cdFx0XHQnYm9keSc6ICQoICdib2R5JyApLFxuXHRcdFx0J21lbnVQYXJlbnQnOiAkKCcubWVudS1pdGVtLWhhcy1jaGlsZHJlbicpXG5cdFx0fTtcblx0fTtcblx0YXBwLm1lbnVQYXJlbnRUYXJnZXQgPSBmdW5jdGlvbiAoKSB7XG4vLyBcdFx0Y29uc29sZS5sb2cod2luZG93LkhhbW1lcik7XG4vLyBcdFx0IGFwcC4kYy5tZW51UGFyZW50Lm9uKCd0b3VjaHN0YXJ0JywgZnVuY3Rpb24oKXtldmVudC5wcmV2ZW50RGVmYXVsdCgpOyAkKHRoaXMpLmZpbmQoJy5zdWItbWVudSBsaScpLmFkZENsYXNzKCdtZW51LWhvdmVyLWJlaGF2aW9yJyk7IGNvbnNvbGUubG9nKHRoaXMpfSApO1xuLy8gXHRcdFx0Y29uc29sZS5sb2coXCJzXCIpO1xuLy8gXHRcdHZhciBoYW1tZXJ0aW1lID0gbmV3IHdpbmRvdy5IYW1tZXIoYXBwLiRjLm1lbnVQYXJlbnQpO1xuLy8gaGFtbWVydGltZS5vbigndGFwJywgZnVuY3Rpb24oZXYpIHtcbi8vIFx0Y29uc29sZS5sb2coZXYpO1xuLy8gfSk7XG4gJCgnLmFydGlzdC1zdWItaXRlbSAuc3ViLW1lbnUnKS5hZGRDbGFzcygnZHJvcGRvd24taGlkZScpO1xuJCgnLmFydGlzdC1zdWItaXRlbSBhJykubW91c2VlbnRlcihmdW5jdGlvbiAoKSB7XG5cdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdCAvLyQoJy5hcnRpc3Qtc3ViLWl0ZW0gLnN1Yi1tZW51JykuY3NzKCdkaXNwbGF5JywnJyk7XG4gICAgLy8gJCh0aGlzKS5jc3MoJ2JhY2tncm91bmQnLCd5ZWxsb3cnKTtcblx0XHQkKHRoaXMpLmNsb3Nlc3QoJy5hcnRpc3Qtc3ViLWl0ZW0nKS5jaGlsZHJlbihcIi5zdWItbWVudVwiKS5hZGRDbGFzcyhcImRyb3Bkb3duLW9wZW5cIikucmVtb3ZlQ2xhc3MoXCJkcm9wZG93bi1oaWRlXCIpO1xuXHRjb25zb2xlLmxvZygkKHRoaXMpKTtcblxufSk7XG4kKCcuYXJ0aXN0LXN1Yi1pdGVtIC5zdWItbWVudScpLm1vdXNlbGVhdmUoZnVuY3Rpb24gKCkge1xuXHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAkKHRoaXMpLmNzcygnYmFja2dyb3VuZCcsJ3llbGxvdycpO1xuXHQvL1x0JCh0aGlzKS5jaGlsZHJlbignLnN1Yi1tZW51JykucmVtb3ZlQ2xhc3MoJ2Ryb3Bkb3duLW9wZW4nKS5hZGRDbGFzcygnZHJvcGRvd24taGlkZScpO1xuXHRcdCQodGhpcykucmVtb3ZlQ2xhc3MoJ2Ryb3Bkb3duLW9wZW4nKS5hZGRDbGFzcygnZHJvcGRvd24taGlkZScpO1xuXHRjb25zb2xlLmxvZygkKHRoaXMpKTtcblxufSk7XG5cbiAgLy8gd2luZG93IHdpZHRoIGlzIGF0IGxlYXN0IDUwMHB4XG4gIC8vIHdpbmRvdyB3aWR0aCBpcyBsZXNzIHRoYW4gNTAwcHhcbn1cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHQvLyAvLyAvLyBhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbiAoKSB7XG5cdC8vIC8vIC8vIFx0cmV0dXJuICQoICcuc2VhcmNoLWZpZWxkJyApLmxlbmd0aDtcblx0Ly8gLy8gfTtcblx0Ly8gLy9cblx0Ly8gLy8gLy8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHQvLyAvLyBhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0Ly8gLy8gXHQvLyBSZW1vdmUgcGxhY2Vob2xkZXIgdGV4dCBmcm9tIHNlYXJjaCBmaWVsZCBvbiBmb2N1cy5cblx0Ly8gLy8gXHRhcHAuJGMuYm9keS5vbiggJ2ZvY3VzJywgJy5zZWFyY2gtZmllbGQnLCBhcHAucmVtb3ZlUGxhY2Vob2xkZXJUZXh0ICk7XG5cdC8vIC8vXG5cdC8vIFx0Ly8gQWRkIHBsYWNlaG9sZGVyIHRleHQgYmFjayB0byBzZWFyY2ggZmllbGQgb24gYmx1ci5cblx0Ly8gXHRhcHAuJGMuYm9keS5vbiggJ2JsdXInLCAnLnNlYXJjaC1maWVsZCcsIGFwcC5hZGRQbGFjZWhvbGRlclRleHQgKTtcblx0Ly8gfTtcblx0Ly9cblx0Ly8gLy8gUmVtb3ZlIHBsYWNlaG9sZGVyIHRleHQgZnJvbSBzZWFyY2ggZmllbGQuXG5cdC8vIGFwcC5yZW1vdmVQbGFjZWhvbGRlclRleHQgPSBmdW5jdGlvbiAoKSB7XG5cdC8vIFx0dmFyICRzZWFyY2hfZmllbGQgPSAkKCB0aGlzICk7XG5cdC8vXG5cdC8vIFx0JHNlYXJjaF9maWVsZC5kYXRhKCAncGxhY2Vob2xkZXInLCAkc2VhcmNoX2ZpZWxkLmF0dHIoICdwbGFjZWhvbGRlcicgKSApLmF0dHIoICdwbGFjZWhvbGRlcicsICcnICk7XG5cdC8vIH07XG5cdC8vXG5cdC8vIC8vIFJlcGxhY2UgcGxhY2Vob2xkZXIgdGV4dCBmcm9tIHNlYXJjaCBmaWVsZC5cblx0Ly8gYXBwLmFkZFBsYWNlaG9sZGVyVGV4dCA9IGZ1bmN0aW9uICgpIHtcblx0Ly8gXHR2YXIgJHNlYXJjaF9maWVsZCA9ICQoIHRoaXMgKTtcblx0Ly9cblx0Ly8gXHQkc2VhcmNoX2ZpZWxkLmF0dHIoICdwbGFjZWhvbGRlcicsICRzZWFyY2hfZmllbGQuZGF0YSggJ3BsYWNlaG9sZGVyJyApICkuZGF0YSggJ3BsYWNlaG9sZGVyJywgJycgKTtcblx0Ly8gfTtcblx0Ly9cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xufSApKCB3aW5kb3csIGpRdWVyeSwgd2luZG93LnJlc3BvbnNpdmV2diApO1xuIiwiLyoqXG4gKiBGaWxlIHNlYXJjaC5qc1xuICpcbiAqIERlYWwgd2l0aCB0aGUgc2VhcmNoIGZvcm0uXG4gKi9cbndpbmRvdy53ZHNTZWFyY2ggPSB7fTtcblxuKCBmdW5jdGlvbiAoIHdpbmRvdywgJCwgYXBwICkge1xuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLmNhY2hlKCk7XG5cblx0XHRpZiAoIGFwcC5tZWV0c1JlcXVpcmVtZW50cygpICkge1xuXHRcdFx0YXBwLmJpbmRFdmVudHMoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gQ2FjaGUgYWxsIHRoZSB0aGluZ3MuXG5cdGFwcC5jYWNoZSA9IGZ1bmN0aW9uICgpIHtcblx0XHRhcHAuJGMgPSB7XG5cdFx0XHQnYm9keSc6ICQoICdib2R5JyApXG5cdFx0fTtcblx0fTtcblxuXHQvLyBEbyB3ZSBtZWV0IHRoZSByZXF1aXJlbWVudHM/XG5cdGFwcC5tZWV0c1JlcXVpcmVtZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gJCggJy5zZWFyY2gtZmllbGQnICkubGVuZ3RoO1xuXHR9O1xuXG5cdC8vIENvbWJpbmUgYWxsIGV2ZW50cy5cblx0YXBwLmJpbmRFdmVudHMgPSBmdW5jdGlvbiAoKSB7XG5cdFx0Ly8gUmVtb3ZlIHBsYWNlaG9sZGVyIHRleHQgZnJvbSBzZWFyY2ggZmllbGQgb24gZm9jdXMuXG5cdFx0YXBwLiRjLmJvZHkub24oICdmb2N1cycsICcuc2VhcmNoLWZpZWxkJywgYXBwLnJlbW92ZVBsYWNlaG9sZGVyVGV4dCApO1xuXG5cdFx0Ly8gQWRkIHBsYWNlaG9sZGVyIHRleHQgYmFjayB0byBzZWFyY2ggZmllbGQgb24gYmx1ci5cblx0XHRhcHAuJGMuYm9keS5vbiggJ2JsdXInLCAnLnNlYXJjaC1maWVsZCcsIGFwcC5hZGRQbGFjZWhvbGRlclRleHQgKTtcblx0fTtcblxuXHQvLyBSZW1vdmUgcGxhY2Vob2xkZXIgdGV4dCBmcm9tIHNlYXJjaCBmaWVsZC5cblx0YXBwLnJlbW92ZVBsYWNlaG9sZGVyVGV4dCA9IGZ1bmN0aW9uICgpIHtcblx0XHR2YXIgJHNlYXJjaF9maWVsZCA9ICQoIHRoaXMgKTtcblxuXHRcdCRzZWFyY2hfZmllbGQuZGF0YSggJ3BsYWNlaG9sZGVyJywgJHNlYXJjaF9maWVsZC5hdHRyKCAncGxhY2Vob2xkZXInICkgKS5hdHRyKCAncGxhY2Vob2xkZXInLCAnJyApO1xuXHR9O1xuXG5cdC8vIFJlcGxhY2UgcGxhY2Vob2xkZXIgdGV4dCBmcm9tIHNlYXJjaCBmaWVsZC5cblx0YXBwLmFkZFBsYWNlaG9sZGVyVGV4dCA9IGZ1bmN0aW9uICgpIHtcblx0XHR2YXIgJHNlYXJjaF9maWVsZCA9ICQoIHRoaXMgKTtcblxuXHRcdCRzZWFyY2hfZmllbGQuYXR0ciggJ3BsYWNlaG9sZGVyJywgJHNlYXJjaF9maWVsZC5kYXRhKCAncGxhY2Vob2xkZXInICkgKS5kYXRhKCAncGxhY2Vob2xkZXInLCAnJyApO1xuXHR9O1xuXG5cdC8vIEVuZ2FnZSFcblx0JCggYXBwLmluaXQgKTtcbn0gKSggd2luZG93LCBqUXVlcnksIHdpbmRvdy53ZHNTZWFyY2ggKTtcbiIsIi8qKlxuICogRmlsZSBza2lwLWxpbmstZm9jdXMtZml4LmpzLlxuICpcbiAqIEhlbHBzIHdpdGggYWNjZXNzaWJpbGl0eSBmb3Iga2V5Ym9hcmQgb25seSB1c2Vycy5cbiAqXG4gKiBMZWFybiBtb3JlOiBodHRwczovL2dpdC5pby92V2RyMlxuICovXG4oIGZ1bmN0aW9uICgpIHtcblx0dmFyIGlzV2Via2l0ID0gbmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpLmluZGV4T2YoICd3ZWJraXQnICkgPiAtMSxcblx0XHRpc09wZXJhID0gbmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpLmluZGV4T2YoICdvcGVyYScgKSA+IC0xLFxuXHRcdGlzSWUgPSBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZiggJ21zaWUnICkgPiAtMTtcblxuXHRpZiAoICggaXNXZWJraXQgfHwgaXNPcGVyYSB8fCBpc0llICkgJiYgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQgJiYgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIgKSB7XG5cdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoICdoYXNoY2hhbmdlJywgZnVuY3Rpb24gKCkge1xuXHRcdFx0dmFyIGlkID0gbG9jYXRpb24uaGFzaC5zdWJzdHJpbmcoIDEgKSxcblx0XHRcdFx0ZWxlbWVudDtcblxuXHRcdFx0aWYgKCAhKCAvXltBLXowLTlfLV0rJC8gKS50ZXN0KCBpZCApICkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggaWQgKTtcblxuXHRcdFx0aWYgKCBlbGVtZW50ICkge1xuXHRcdFx0XHRpZiAoICEoIC9eKD86YXxzZWxlY3R8aW5wdXR8YnV0dG9ufHRleHRhcmVhKSQvaSApLnRlc3QoIGVsZW1lbnQudGFnTmFtZSApICkge1xuXHRcdFx0XHRcdGVsZW1lbnQudGFiSW5kZXggPSAtMTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGVsZW1lbnQuZm9jdXMoKTtcblx0XHRcdH1cblx0XHR9LCBmYWxzZSApO1xuXHR9XG59ICkoKTtcbiIsIi8qKlxuICogRmlsZSB3aW5kb3ctcmVhZHkuanNcbiAqXG4gKiBBZGQgYSBcInJlYWR5XCIgY2xhc3MgdG8gPGJvZHk+IHdoZW4gd2luZG93IGlzIHJlYWR5LlxuICovXG53aW5kb3cud2RzV2luZG93UmVhZHkgPSB7fTtcbiggZnVuY3Rpb24gKCB3aW5kb3csICQsIGFwcCApIHtcblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXHRcdGFwcC5iaW5kRXZlbnRzKCk7XG5cdH07XG5cblx0Ly8gQ2FjaGUgZG9jdW1lbnQgZWxlbWVudHMuXG5cdGFwcC5jYWNoZSA9IGZ1bmN0aW9uICgpIHtcblx0XHRhcHAuJGMgPSB7XG5cdFx0XHQnd2luZG93JzogJCggd2luZG93ICksXG5cdFx0XHQnYm9keSc6ICQoIGRvY3VtZW50LmJvZHkgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHRhcHAuJGMud2luZG93LmxvYWQoIGFwcC5hZGRCb2R5Q2xhc3MgKTtcblx0fTtcblxuXHQvLyBBZGQgYSBjbGFzcyB0byA8Ym9keT4uXG5cdGFwcC5hZGRCb2R5Q2xhc3MgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLiRjLmJvZHkuYWRkQ2xhc3MoICdyZWFkeScgKTtcblx0fTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG59ICkoIHdpbmRvdywgalF1ZXJ5LCB3aW5kb3cud2RzV2luZG93UmVhZHkgKTtcbiJdfQ==
